import { HomePage } from "../support/pages/home.page";
import { LoginPage } from "../support/pages/login.page";
import { wait2SecondsForPageToLoad } from "../support/util";

describe(`Login test for mariacasino.com`, function() {

    const loginPage = new LoginPage();
    const homePage = new HomePage();

    beforeEach(function() {
        cy.fixture('/test-data.json').as('data');
        homePage
            .openHomePageAndRemovePopUp();
    });

    afterEach(function() {
        // Adding the manual clearance of all cookies because of the Cypress Firefox issue: https://github.com/cypress-io/cypress/issues/6375
        cy.getCookies()
            .then((cookies) => cookies.forEach(cookie => cy.clearCookie(cookie.name)));
    });

    it(`Should assert the data on the Login form`, function() {
        homePage
            .assertLoginButtonLabel(this.data.label.logIn)
            .clickOnLoginButton();
        loginPage
            // Assert labels
            .checkContentOfLoginForm(this.data.label.login)
            .checkContentOfLoginForm(this.data.label.forgot)
            .checkContentOfLoginForm(this.data.label.logIn)
            .checkContentOfLoginForm(this.data.label.register)
            // Assert Email field attributes
            .assertFieldAttribute(0, this.data.attribute.name.type, this.data.attribute.value.email)
            .assertFieldAttribute(0, this.data.attribute.name.kind, this.data.attribute.value.base)
            .assertFieldAttribute(0, this.data.attribute.name.placeholder, this.data.attribute.value.emailUsername)
            .assertFieldAttribute(0, this.data.attribute.name.autocomplete, this.data.attribute.value.false)
            .assertFieldAttribute(0, this.data.attribute.name.autocorrect, this.data.attribute.value.none)
            .assertFieldAttribute(0, this.data.attribute.name.autocapitalize, this.data.attribute.value.none)
            // Assert password field attributes
            .assertFieldAttribute(1, this.data.attribute.name.type, this.data.attribute.value.password)
            .assertFieldAttribute(1, this.data.attribute.name.kind, this.data.attribute.value.base)
            .assertFieldAttribute(1, this.data.attribute.name.placeholder, this.data.attribute.value.passwordP)
            .assertFieldAttribute(1, this.data.attribute.name.autocomplete, this.data.attribute.value.false)
            .assertFieldAttribute(1, this.data.attribute.name.autocorrect, this.data.attribute.value.none)
            .assertFieldAttribute(1, this.data.attribute.name.autocapitalize, this.data.attribute.value.none)
            // Close form
            .closeLoginForm();
    });

    it(`Should click on "Forgot" button on the Login form and assert the shown data`, function() {
        cy.setRouteAsAliasPOST(`/${this.data.login.wrong.mail}@maria`, 'fakeMailAlias')

        homePage
            .clickOnLoginButton();
        loginPage
            .clickOnForgotButton()
            // Assert labels
            .checkContentOnForgotWindow(this.data.resetPassword.labels)
            // Assert Email field attributes
            .assertFieldAttribute(0, this.data.attribute.name.type, this.data.attribute.value.email)
            .assertFieldAttribute(0, this.data.attribute.name.placeholder, this.data.attribute.value.emailUsernameWhitespace) // Potential issue: inconsistency in placeholder namings
            .assertFieldAttribute(0, this.data.attribute.name.autocomplete, this.data.attribute.value.false)
            .assertFieldAttribute(0, this.data.attribute.name.autocorrect, this.data.attribute.value.none)
            .assertFieldAttribute(0, this.data.attribute.name.autocapitalize, this.data.attribute.value.none)
            // Assert back button functionality
            .clickOnBackButton()
            .clickOnForgotButton()
            // Assert Send button
            .assertSendButtonIsDisabled()
            .typeInField(0, this.data.login.wrong.mail)
            .clickOnSendButton()
            .waitForDataLoad('fakeMailAlias')
            // Assert Reset password info
            .checkContentOnForgotWindow(this.data.resetPassword.text)
            // Click to close Reset password info and assert that first page is shown
            .clickOnLink()
            .checkContentOfLoginForm(this.data.label.forgot)
            // Close form
            .closeLoginForm();
    });

    it(`Should test the login functionality`, function() {
        cy.setRouteAsAliasPOST('/login-api/methods/password*', 'passwordAlias')
        cy.setRouteAsAliasPOST('/v1/events', 'eventsAlias')
        cy.setRouteAsAliasPOST('/login-api/logout', 'logoutAlias')

        homePage
            .clickOnLoginButton();
        loginPage
            // Try to log in with incorrect password and assert error message(s)
            .typeInField(0, Cypress.env('email'))
            .typeInField(1, this.data.login.wrong.password)
            .clickOnLoginButton()
            .waitForDataLoad('passwordAlias')
            .assertBothPotentialErrorMessagesOnLoginForm(this.data.errorMessages[0], this.data.errorMessages[1])
            // Assert Show password button
            .assertFieldAttribute(1, this.data.attribute.name.type, this.data.attribute.value.password)
            .clickOnShowPasswordButton()
            .assertFieldAttribute(1, this.data.attribute.name.type, this.data.attribute.value.text)
            // Try to log in with the correct password (and email address)
            .typeInField(1, Cypress.env('password'))
            .clickOnLoginButton()
            .waitForDataLoad('passwordAlias')
            .wait(wait2SecondsForPageToLoad);
        homePage
            // Assert account balance and log out
            .assertAccountBalance(this.data.accountBalance.eur)
            .waitForDataLoad('eventsAlias')
            .clickOnLogoutButton()
            .assertUrl(String(Cypress.config().baseUrl))
            .waitForDataLoad('logoutAlias')
            .wait(wait2SecondsForPageToLoad)
            // Log in with another user
            .clickOnLoginButton();
        loginPage
            .typeInField(0, Cypress.env('email2'))
            .typeInField(1, Cypress.env('password2'))
            .clickOnLoginButton()
            .waitForDataLoad('passwordAlias')
            .wait(wait2SecondsForPageToLoad);
        homePage
            // Assert account balance and log out
            .assertAccountBalance(this.data.accountBalance.cad)
            .waitForDataLoad('eventsAlias')
            .clickOnLogoutButton()
            .assertUrl(String(Cypress.config().baseUrl))
            .waitForDataLoad('logoutAlias');
    });
})
