Cypress.Commands.add('getName', (label) => {
    cy.get(`[data-test-name="${label}"]`);
});

Cypress.Commands.add('getButton', (label) => {
    cy.get(`[data-tracking-button="${label}"]`);
});

Cypress.Commands.add('setRouteAsAliasPOST', (route, alias) => {
    cy.intercept('POST', `**/${route}`).as(`${alias}`);
});
