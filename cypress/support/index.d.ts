declare namespace Cypress {
    interface Chainable {
        // Custom command to select DOM element by data-test-name attribute.
        // Example: cy.getName('forgot-password')
        getName(label: string): Chainable<Element>

        // Custom command to select DOM element by data-tracking-button attribute.
        // Example: cy.getButton('Send')
        getButton(label: string): Chainable<Element>

        // Custom command to set alias to POST request.
        // Example: cy.setRouteAsAliasPOST('/loremipsum@maria', 'fakeMailAlias')
        setRouteAsAliasPOST(route: string, alias: string): Chainable<Element>
    }
}
