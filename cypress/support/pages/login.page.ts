import { Page } from "./page";

export class LoginPage extends Page {

    checkContentOfLoginForm(content: string): this {
        cy.getName('login-form')
            .contains(content);
        return this;
    }

    assertBothPotentialErrorMessagesOnLoginForm(firstMessage: string, secondMessage: string): this {
        // Should contain firstMessage or secondMessage
        // g modifier: global. All matches (don't return after first match)
        const regex = new RegExp(firstMessage + '|' + secondMessage, 'g');
        cy.getName('login-form')
            .contains(regex);
        return this;
    }

    closeLoginForm(): this {
        cy.getName('modal-login-close')
            .click();
        return this;
    }

    assertFieldAttribute(position: number, attribute: string, value: string): this {
        cy.getName('inputfield')
            .eq(position)
            .find('.sc-bwCtUz')
            .should('have.attr', attribute, value);
        return this;
    }

    clickOnForgotButton(): this {
        cy.getName('forgot-password-link')
            .click();
        return this;
    }

    checkContentOnForgotWindow(content: string[]): this {
        for (const label of content) {
            cy.getName('forgot-password')
            .contains(label);
        }
        return this;
    }

    clickOnBackButton(): this {
        cy.get('.sc-jxgvnK')
            .click();
        return this;
    }

    assertSendButtonIsDisabled(): this {
        cy.getButton('Send')
            .should('have.attr', 'disabled');
        return this;
    }

    clickOnSendButton(): this {
        cy.getButton('Send')
            .click();
        return this;
    }

    typeInField(position: number, text: string): this {
        cy.getName('inputfield')
            .eq(position)
            .find('.sc-bwCtUz')
            .clear()
            .type(text);
        return this;
    }

    clickOnLink(): this {
        cy.getName('link')
            .click();
        return this;
    }

    clickOnLoginButton(): this {
        cy.getName('EnabledLoginButton')
            .click();
        return this;
    }

    clickOnShowPasswordButton(): this {
        cy.getName('ShowPasswordButton')
            .click();
        return this;
    }
}
