export class Page {
    
    // Chainable implicit wait
    wait(time: number): this {
        cy.wait(time);
        return this;
    }

    // Chainable explicit wait
    waitForDataLoad(alias: string): this {
        cy.wait(`@${alias}`);
        return this;
    }

    assertUrl(url: string): this {
        cy.url()
            .should('eq', url);
        return this;
    }
}
