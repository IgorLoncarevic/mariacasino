import { Page } from "./page";

export class HomePage extends Page {
    
    openHomePageAndRemovePopUp(): this {
        cy.visit('');
        cy.contains('This website uses cookies', {timeout: 20000}).should('exist');
        cy.contains('OK').click();
        return this;
    }

    assertLoginButtonLabel(label: string): this {
        cy.getName('login-button')
            .contains(label);
        return this;
    }

    clickOnLoginButton(): this {
        cy.getName('login-button')
            .click();
        return this;
    }

    clickOnLogoutButton(): this {
        cy.getName('footer-logout')
            .click();
        return this;
    }

    assertAccountBalance(balance: string): this {
        cy.getName('header-balance-deposit-button')
            .contains(balance);
        return this;
    }
}
