import './commands'

// Some resources fail to load while opening the application - ignore them
Cypress.on('uncaught:exception', () => {
    // Returning false here prevents Cypress from failing the test
    return false;
})
