# Automated test - Login to Maria Casino

### [mariacasino](https://www.mariacasino.com/) 

## Description

The login.spec.ts file contains three tests:

1. Data assertion on Login form
2. Data assertion for Forgot login functionality
3. Test login with incorrect and correct credentials

## Usage

Run login.spec.ts file using Cypress:

- npm run cypress:open - run headed and select browser
- npm run cypress:run:chrome - run headless Chrome
- npm run cypress:run:firefox - run headless Firefox
